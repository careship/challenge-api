<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<=100;$i++) {
            $comment = new \App\Comment();
            $comment->user_id = mt_rand(1,5);
            $comment->comment = file_get_contents(app_path('../storage/texts/'.$i.'.txt'));
            $comment->save();
        }
    }
}
