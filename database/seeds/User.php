<?php

use Illuminate\Database\Seeder;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(['a','b','c','d','e'] as $name) {
            $user = new \App\User();
            $user->name = $name.'@example.com';
            $user->email = $name.'@example.com';
            $user->password = bcrypt($name);
            $user->save();
        }
    }
}
