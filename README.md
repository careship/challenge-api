# Overview

In the repository is a simple REST API. To Run the server go to the directory and run artisan serve:

### install

```
composer install
```

### run test server
```
cd ~/workspace/test
php artisan serve
```

The project uses sqlite as datastore and contains two models, users and comments.

It uses Laravel 5.3, docs can be found at [https://laravel.com/docs/5.3]

 
## Tables
### Users
 * id (integer)
 * name (varchar)
 * email (varchar)
 * password (varchar)
 * remember_token (varchar)
 * created_at (datetime)
 * updated_at (datetime)
 
### Comments
 * id (integer)
 * user_id (integer)
 * comment (text)
 * created_at (datetime)
 * updated_at (datetime)
 
## Authorization

The API uses HTTP Base Auth for authorization, the existing users are in the format `a@example.com` with password `a`.


## Challenge #1
An endpoint for each user to get their latest five comments, there should be a pagination to receive also older comments.

Response should be a JSON-Array, containing the comments

## Challenge #2
Two endpoints to get the "happiest" (count of usage of the word `good` ) and "unhappiest" (count of usage of the word `bad` ) 20% of customers